
var routes = [
  {
    path: '/',
    url:'./index.html',
    on:{
      pageBeforeIn:function(page){
        if(user.id == 0){
          //alert('cazzo');
        }
      }
    }
  },
  {
    path:'/report/',
    componentUrl:'./pages/report.html',
    on:{
      pageAfterIn:function(page){
        showMapNewPort();
        loadAnimals(page);

      }
    }
  },
  {
    path:'/report-detail/',
    componentUrl:'./pages/report-detail.html',
    on:{
      pageAfterIn:function(page){


      }
    }
  },
  {
    path:'/contact-us/',
    componentUrl:'./pages/contact-us.html',
    on:{
      pageAfterIn:function(page){

      }
    }
  },
  {
    path: '/ranking/',
    url:'./pages/ranking.html',
    on:{
      pageAfterIn:function(page){

        app.request.json(apiPrefix+'users-ranking',{} , function (response) {
          var template = $$('#rankingPageTemplate').html();
          var compiledTemplate = Template7.compile(template);
          var context = {
            dictionary:dictionary,
            users:response
          };
          var html = compiledTemplate(context);
          $$('#rankingPage').html(html);
        })

      }
    }
  },
  {
    path: '/map/',
    componentUrl:'./pages/map.html',
    on:{
      pageAfterIn:function(page){
        showMapReportsList();
      }
    }
  },
  {
    path: '/dashboard/',
    componentUrl:'./pages/dashboard.html',
    on:{
      pageAfterIn:function(page){

      }
    }
  },
  {
    path: '/report-thank-you/',
    componentUrl:'./pages/report-thank-you.html',
    on:{
      pageAfterIn:function(page){
        setTimeout(function(){
          mainView.router.navigate('/dashboard/',{context:{}});
        },3000);
      }
    }
  },
  {
    path: '/sign-in',
    componentUrl: './pages/sign-in.html',
    on:{

      pageAfterIn:function(page){
        $$('.toolbar-bottom').hide();
        $$('#sign-in-button').click(function(){
          var getData = { email:$$('#sign-in-email').val(), password: $$('#sign-in-password').val() };
          app.request.json(apiPrefix+'sign-in',getData , function (response) {
            //alert(JSON.stringify(response));
            WonderPush.subscribeToNotifications();
            if(response.status == 1){
              user = response.data;
              localStorage.setItem('user', JSON.stringify(user));
              mainView.router.navigate('/dashboard/',{context:{}});
            }else{

              var toastIcon = app.toast.create({
                icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                text: response.err_message,
                position: 'center',
                closeTimeout: 2000,
              });
              toastIcon.open();
            }

          });

        })
      }
    }
  },
  {
    path: '/sign-up',
    componentUrl: './pages/sign-up.html',
    on:{
      pageAfterIn:function(page){
        $$('#submitSignUpForm').click(function(){
          var formData = app.form.convertToData('#signUpForm');

          app.request({
            dataType:'json',
            data:formData,
            url:apiPrefix+'sign-up',
            statusCode:{
              422:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.check_mandatory_fields,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              423:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.user_already_registered,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              424:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.repeated_password_does_not_match,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              200:function(response){
                console.log(response);
              }
            }
          })
        })
      }
    }
  },
  {
    path: '/about/',
    url: './pages/about.html',
  },
  {
    path: '/form/',
    url: './pages/form.html',
  },
  {
    path: '/catalog/',
    componentUrl: './pages/catalog.html',
  },
  {
    path: '/product/:id/',
    componentUrl: './pages/product.html',
  },
  {
    path: '/settings/',
    url: './pages/settings.html',
    on:{
      pageAfterIn:function(page){
        var template = $$('#settingsPageTemplate').html();
        var compiledTemplate = Template7.compile(template);
        var context = {
          user:user,
          dictionary:dictionary
        };
        var html = compiledTemplate(context);
        $$('#settingsPage').html(html);



        $$('#submitSettingsForm').click(function(){
          alert('cazzoo');
          var formData = app.form.convertToData('#settingsForm');
          alert(JSON.stringify(formData));
          app.request({
            dataType:'json',
            data:formData,
            url:apiPrefix+'user-update',
            statusCode:{
              422:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.check_mandatory_fields,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              423:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.user_already_registered,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              424:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">alert</i>' : '<i class="material-icons">alert</i>',
                  text: dictionary.repeated_password_does_not_match,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              },
              200:function(response){
                var toastIcon = app.toast.create({
                  icon: app.theme === 'ios' ? '<i class="f7-icons">checkmark_alt_circle</i>' : '<i class="material-icons">checkmark_alt_circle</i>',
                  text: dictionary.user_data_updated,
                  position: 'center',
                  closeTimeout: 2000,
                });
                toastIcon.open();
              }
            }
          })
        })



      }
    }
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    componentUrl: './pages/dynamic-route.html',
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: './pages/request-and-load.html',
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
