var dictionary = {
  'it-it' : {
    'surname' : 'Cognome',
    'name' : 'Nome',
    'email' : 'Email',
    'password' : 'Password',
    'repeat_password' : 'Ripeti password',
    'phone' : 'Telefono',
    'genderd' : 'Genere',
    'date_of_birth' : 'Data di nascita',
    'username':'Nome utente',
    'sign_in':'Accedi',
    'sign_up':'Registrati',
    'login':'Entra',
    'check_mandatory_fields':'Controlla i campi obbligatori',
    'user_already_registered':'L\'indirizzo email appartiene già ad un altro utente',
    'repeated_password_does_not_match':'Le password inserite non corrispondono',
    'new_report':'Nuovo report',
    'user_data_updated':'Dati dell\'utente aggiornati correttamente!'
  }
}
