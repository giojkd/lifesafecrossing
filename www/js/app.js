var $$ = Dom7;
var user = {};
localStorage.removeItem('user'); /* <-- remove user for development purposes */

var lang = 'it-it';
var reportFiles = [];
var reportsListMap,newReportMap;
//var animals = [];
dictionary = dictionary[lang];


if(localStorage.getItem("user") === null){
  user = {
    id:0
  };
}else{
  user = JSON.parse(localStorage.getItem('user'));
}

console.log(user);

//user.id = 1; /* <-- remove user for development purposes */


var apiPrefix = 'http://appalife.amtitalia.com/';



apiPrefix += '/api/';

var app = new Framework7({
  root: '#app', // App root element
  id: 'com.amtitalia.lifesafecrossing', // App bundle ID
  name: 'LifeSafeCrossing', // App name
  theme: 'auto', // Automatic theme detection
  // App root data
  data: function () {
    return {
      user: {
        firstName: 'John',
        lastName: 'Doe',
      },
      dictionary:dictionary,
      products: [

      ],
      animals:animals
    };
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  // App routes
  routes: routes,



  // Input settings
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  // Cordova Statusbar settings
  statusbar: {
    overlay: Framework7.device.cordova && Framework7.device.ios || 'auto',
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },
  on: {
    pageInit: function() {
      if(user.id == 0){
        app.views.main.router.navigate('/sign-in/');
      }
    },
    pageBeforeIn:function(){
      $$('.toolbar-bottom').show();
    },
    mounted:function(){

    },
    init: function () {
      var f7 = this;

      console.log('device ready');


      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);


      }
    },
  },
});

// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close('#my-login-screen');

  // Alert username and password
  app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
});


var mainView = app.views.create('#view-home');

function centerMap(){

  var amtPosition = {lat:43.788360,lng:11.231300};
  newReportMap.setCameraTarget(amtPosition).setCameraZoom(16);
  return;


  navigator.geolocation.getCurrentPosition(function(position){
    /*
    alert('Latitude: '          + position.coords.latitude          + '\n' +
    'Longitude: '         + position.coords.longitude         + '\n' +
    'Altitude: '          + position.coords.altitude          + '\n' +
    'Accuracy: '          + position.coords.accuracy          + '\n' +
    'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
    'Heading: '           + position.coords.heading           + '\n' +
    'Speed: '             + position.coords.speed             + '\n' +
    'Timestamp: '         + position.timestamp                + '\n');*/
    var centerPosition = {lat:position.coords.latitude,lng:position.coords.longitude};
    newReportMap.setCameraTarget(centerPosition).setCameraZoom(16);

  }, function(){
    alert('error');
  });


}




function onError(error) {
  alert('code: '    + error.code    + '\n' +
  'message: ' + error.message + '\n');
}

document.addEventListener("deviceready", function(){
  centerMap();

}, false);

function loadAnimals(page){
  app.request.json(apiPrefix+'animals',{} , function (response) {
    animals = response;







    var template = '<a class="item-link smart-select" data-open-in="popup"  data-searchbar="true" data-searchbar-placeholder="Search animal"><select name="report_row[animal_id]">{{#each animals}}<optgroup label="{{name}}">{{#each animals}}<option value="{{id}}">{{name}}</option>{{/each}}</optgroup>{{/each}}</select><div class="item-content"><div class="item-inner"><div class="item-title">Animal</div></div></div></a>';

    // compile it with Template7
    var compiledTemplate = Template7.compile(template);

    // Now we may render our compiled template by passing required context
    var context = {
      animals: animals
    };
    var html = compiledTemplate(context);


    $$('#animalsSmartSelect').html(html);

    //smartSelect.renderItems();
    //page.emit('page:init');
    //$$('#animalsSmartSelect').renderPopup();
  })
}

function showMapReportsList(){
  /*custom_map_canvas_reports_list*/
  plugin.google.maps.environment.setEnv({
    'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBqKgUEcdSp749twSvTg7GBH-oQnI7US_E',
    'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBqKgUEcdSp749twSvTg7GBH-oQnI7US_E'
  });
  var div = document.getElementById("custom_map_canvas_reports_list");
  reportsListMap = plugin.google.maps.Map.getMap(div);

  reportsListMap.on(plugin.google.maps.event.CAMERA_MOVE_END, function(latLng) {
    //alert(JSON.stringify(reportsListMap.getVisibleRegion()));
  });

  app.request.json(apiPrefix+'reports',{} , function (response) {

    var reports = response;
    var markers = new Array();
    for(var i in reports){
      var report = reports[i];

      var myLatLng = {lat: report.lat, lng: report.lng};
      var markerCfg = {
        position: myLatLng,
        map: reportsListMap,
        title: report.animal
      };

      //alert(JSON.stringify(markerCfg));

      //var marker =  new plugin.google.maps.Marker(markerCfg);


      var marker = reportsListMap.addMarker({
        position: {lat: report.lat, lng: report.lng},
        title: report.animal,
        snippet: report.type+' '+report.notes,
        animation: plugin.google.maps.Animation.BOUNCE
      })

      markers[report.id] = marker;

      markers[report.id].showInfoWindow();


      markers[report.id].on(plugin.google.maps.event.MARKER_CLICK, function() {
        alert(markers[report.id]);
      });


    }

  })



}


function showMapNewPort(){
  plugin.google.maps.environment.setEnv({
    'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBqKgUEcdSp749twSvTg7GBH-oQnI7US_E',
    'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBqKgUEcdSp749twSvTg7GBH-oQnI7US_E'
  });
  var div = document.getElementById("custom_map_canvas");
  newReportMap = plugin.google.maps.Map.getMap(div);
  newReportMap.on(plugin.google.maps.event.CAMERA_MOVE_END, function(latLng) {
    var mapCenter = latLng.target;
    $$('#reportForm input[name="lat"]').val(mapCenter.lat);
    $$('#reportForm input[name="lng"]').val(mapCenter.lng);
  });
}

function sendReport(){

  var formData = app.form.convertToData('#reportForm');

  app.request.post(apiPrefix+'report',formData,function(response){
    //    alert(JSON.stringify(response));
    response = JSON.parse(response);
    if(response.status == 1){

      //app.router.navigate("/report-thank-you");
      app.views.main.router.navigate("/report-thank-you/");
    }
  })
}

function removeReportFile(id){
  reportFiles.splice(reportFiles.indexOf(id), 1);
  $$('#reportFile-'+id).remove();
  updateReportFilesList();
}
function updateReportFilesList(){
  $$('#filesList').val(reportFiles.join(','));
}

function cameraTakePicture() {
  navigator.camera.getPicture(onSuccess, onFail, {
    quality: 80,
    destinationType: Camera.DestinationType.DATA_URL
  });
  function onSuccess(imageData) {
    app.request.post(apiPrefix+'file-upload',{imageData:imageData} , function (response) {
      var responseJson = JSON.parse(response);

      var fileId = parseInt(responseJson.id);
      var imageSrc = "data:image/jpeg;base64," + imageData;
      var imageObj = "<div id=\"reportFile-"+fileId+"\" class=\"report-img col-50\" style=\"background-image:url('"+imageSrc+"')\"><a onclick=\"removeReportFile("+fileId+")\" href=\"\">Remove</a></div>";
      reportFiles.push(fileId);
      $$('.imagesList').append(imageObj);
      updateReportFilesList();

    })
  }
  function onFail(message) {
    alert(message);
  }
}
