var animals = [
  {
    "id": 1,
    "created_at": "2019-06-04 15:40:58",
    "updated_at": "2019-06-04 15:41:13",
    "name": "Mammalia",
    "animals": [
      {
        "id": 1,
        "created_at": "2019-06-04 15:43:42",
        "updated_at": "2019-11-08 10:21:03",
        "name": "Orso",
        "animalclass_id": 1,
        "scientific_name": "Ursus arctos"
      },
      {
        "id": 3,
        "created_at": "2019-11-11 16:22:51",
        "updated_at": null,
        "name": "Lupo",
        "animalclass_id": 1,
        "scientific_name": "Canis Lupus"
      }
    ]
  },
  {
    "id": 2,
    "created_at": "2019-06-04 15:41:20",
    "updated_at": null,
    "name": "Chondrichthyes",
    "animals": [

    ]
  },
  {
    "id": 3,
    "created_at": "2019-06-04 15:41:25",
    "updated_at": null,
    "name": "Osteichthyes",
    "animals": [

    ]
  },
  {
    "id": 4,
    "created_at": "2019-06-04 15:41:29",
    "updated_at": null,
    "name": "Amphibia",
    "animals": [

    ]
  },
  {
    "id": 5,
    "created_at": "2019-06-04 15:41:33",
    "updated_at": null,
    "name": "Aves",
    "animals": [

    ]
  },
  {
    "id": 6,
    "created_at": "2019-06-04 15:41:37",
    "updated_at": null,
    "name": "Reptilia",
    "animals": [
      {
        "id": 2,
        "created_at": "2019-06-04 15:43:51",
        "updated_at": "2019-11-08 10:22:20",
        "name": "Vipera",
        "animalclass_id": 6,
        "scientific_name": "Vipera aspis"
      }
    ]
  }
];
